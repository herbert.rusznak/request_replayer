'use strict'

const urls = ['*://127.0.0.1/*']
const debug = true

const log = debug ? chrome.extension.getBackgroundPage().console.log : (m) => {}
let requests = new Map()
let form_id_to_request_id = new Map()

chrome.webRequest.onBeforeRequest.addListener((d) => {
    log('request start')
    log(d)

    if (!('requestBody' in d)) return
    if (!('formData' in d.requestBody)) return
    if (!('form_id' in d.requestBody.formData)) return

    const form_id = d.requestBody.formData.form_id[0]

    requests.set(d.requestId, {state: 'started', form_id: form_id})
    form_id_to_request_id.set(form_id, d.requestId)
    log(requests)
    log(form_id_to_request_id)
}, {'urls': urls}, ["requestBody"])

chrome.webRequest.onCompleted.addListener((d) => {
    // complete bedeutet aber auch 3xx, 4xx, 5xx ;)
    log('request success')
    log(d)
    const r = requests.get(d.requestId)
    if (!r) return
    // damit wir irgendwie isoliert prüfen können ob das teil eh funktioniert werfe ich einen 404
    // und behandle den 404er wie einen net::ERR_xxxxxxxx
    if (d.statusCode == 404) {
        r.state = 'failed'
    } else {
        r.state = 'success'
    }
    requests.set(d.requestId, r)
    log(r)
}, {'urls': urls}, [])

chrome.webRequest.onErrorOccurred.addListener((d) => {
    // echte netzwerkfehler wie z.b. kein netzwerk, timeout udgl...
    log('error detected, retry')
    log(d)
    const r = requests.get(d.requestId)
    if (!r) return
    r.state = 'failed'
    requests.set(d.requestId, r)
    log(r)
}, {'urls': urls})

chrome.runtime.onMessageExternal.addListener(
    function (request, sender, sendResponse) {
        if (request.msg_type == 'get_state') {
            log('get state')
            log(request)
            const request_id = form_id_to_request_id.get(request.data.form_id)
            log(request_id)
            sendResponse({state: requests.get(request_id)})
        }
        if (request.msg_type == 'delete_state') {
            log('delete state')
            log(request)
            const request_id = form_id_to_request_id.get(request.data.form_id)
            log(request_id)

            requests.delete(request_id)
            form_id_to_request_id.delete(request.data.form_id)
            sendResponse({msg: 'key deleted'})
        }
    })
