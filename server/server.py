from flask import abort, Flask, request, render_template
import random
import time

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/submit', methods=['POST'])
def submit():
    time.sleep(5)
    if random.randint(0, 1) == 0:
        return 'request done: ' + request.form.get('msg')

    abort(404)


app.run(debug=True, port=5000)
