# install extension
click:
* chrome://extensions
* enable developer mode
* load unpacked
* choose extension dir

# install test server
    python3 -m venv .venv
    source .venv/bin/activate
    pip3 install flask

# run server
    python server.py

